# multipython

It currently contains Python 3.5.5, 3.6.5, and 3.7.0.  To use one or the other just extend it and run `/usr/local/sbin/setup-python <version>[ version...]` and it'll expand your required environments before testing.
